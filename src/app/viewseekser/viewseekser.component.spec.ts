import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewseekserComponent } from './viewseekser.component';

describe('ViewseekserComponent', () => {
  let component: ViewseekserComponent;
  let fixture: ComponentFixture<ViewseekserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewseekserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewseekserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
