import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindjobseekersComponent } from './findjobseeksers.component';

describe('FindjobseekersComponent', () => {
  let component: FindjobseeksersComponent;
  let fixture: ComponentFixture<FindjobseekersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindjobseeksersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindjobseekersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
