import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../_models/index';
import { UserService, JobService, AlertService, AuthenticationService } from '../_services/index';

@Component({
	selector: 'app-findjobseekers',
    moduleId: module.id,
    templateUrl: 'findjobseekers.component.html'
})

export class FindjobseekersComponent implements OnInit {
    currentUser: User;
	userDetails: any=[];
    findJobSeekers: User[] = [];
	user_id: Number = 0;

    constructor(private userService: UserService, private jobService: JobService,private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService)
		{
		/* if(!this.authenticationService.isLoggedIn()){
            this.router.navigate(['/']);
        } */
        this.userDetails = JSON.parse(localStorage.getItem('currentUser'));	
		if(this.userDetails.ResponseCode){
			this.user_id = this.userDetails.data.id;
		}
    }

    ngOnInit() {
        this.FindJobSeekers();
    }

     private FindJobSeekers() {
        this.jobService.getFindJobSeekers(this.user_id).subscribe(
                data => {
					console.log(data);
					this.findJobSeekers = data.data;
                },
                error => {
                    this.alertService.error(error);
                    //this.loading = false;
                });
    }
}