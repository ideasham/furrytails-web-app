import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { My_jobsComponent } from './my_jobs.component';

describe('My_jobsComponent', () => {
  let component: My_jobsComponent;
  let fixture: ComponentFixture<My_jobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ My_jobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(My_jobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
