import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { User } from '../_models/index';
import { UserService, AlertService, AuthenticationService } from '../_services/index';

@Component({
	selector: 'app-my_chat',
    moduleId: module.id,
    //template: "hello hi my chat"
    templateUrl: 'my_chat.component.html'
})

export class My_chatComponent implements OnInit {
	
	jsonUser:any		=   [];
	userDetails:any		=   [];
	chatmessages:any	=   [];
	messages:any		=   [];

	userid:Number		= 0;
	lastMsgId:Number	= 0;
	senderid:Number		= 0;

	api_token: String	= "";

	constructor(private userService: UserService, private route: ActivatedRoute, private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService){

		if(!this.authenticationService.isLoggedIn()){
			this.router.navigate(['/']);
		}
		this.jsonUser = JSON.parse(localStorage.getItem('currentUser'));
		if(this.jsonUser && this.jsonUser.ResponseCode){
		   this.userid = this.jsonUser.data.id; 
		   this.api_token = this.jsonUser.data.api_token;
		}
	}

	ngOnInit() {
		this.getallmessageslist();
	}

	private getMessageDetails(message){
		
		if(message.receiver_id == this.userid){
			this.senderid = message.sender_id;
		} else {
			this.senderid = message.receiver_id;
		}

		this.userService.getChatMessages(this.userid, this.senderid, message.job_id, this.lastMsgId, this.api_token).subscribe(
		chatresponse => {
			for (var i in chatresponse.data){
				var todaydate = new Date();
				var newdate = new Date(chatresponse.data[i].date);

			
				if(todaydate.toDateString() == newdate.toDateString()){
					
				} else {
				}
						
				if(chatresponse.data[i].class == "left"){
					chatresponse.data[i].class = "pull-left";
				} else {
					chatresponse.data[i].class = "pull-right";
				}

				chatresponse.data[i].count = 0;
			}
		
			this.chatmessages = chatresponse.data;

			console.log(this.chatmessages);
			
		},
		error => {
			this.alertService.error(error);
		});
	}

	private getallmessageslist() {
		this.userService.getAllMessages(this.userid, this.api_token).subscribe(
		response => {
			for (var i in response.data){
				var todaydate = new Date();
				var newdate = new Date(response.data[i].date);

			
				if(todaydate.toDateString() == newdate.toDateString()){
					var hours = newdate.getHours();
					var minutes = newdate.getMinutes();
					var ampm = hours >= 12 ? 'pm' : 'am';
				} else {
				}
						

				response.data[i].count = 0;

				if(i == "0"){
					this.getMessageDetails(response.data[i]);
				}
			}
			
				
			this.messages = response.data;

			

		},
		error => {
			this.alertService.error(error);
		});
	}

  

   
}