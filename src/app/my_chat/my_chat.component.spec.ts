import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { My_chatComponent } from './my_chat.component';

describe('My_chatComponent', () => {
  let component: My_chatComponent;
  let fixture: ComponentFixture<My_chatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ My_chatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(My_chatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
