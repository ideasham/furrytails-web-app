import { NgModule }      from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from './material.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// used to create fake backend
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { AlertService, AuthenticationService, UserService } from './_services/index';
import { JobService } from './_services/job.service';
import { HomeComponent } from './home/index';
import { JobsComponent } from './jobs/index';
import { FindjobseekersComponent } from './findjobseekers/index';
import { CreatejobComponent } from './createjob/index';
import { My_chatComponent } from './my_chat/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { ProfileComponent } from './profile/index';
import { My_postsComponent } from './my_posts/index';
import { My_jobsComponent } from './my_jobs/index';
import { ViewjobComponent } from './viewjob/index';
import { TransactionsComponent } from './transactions/index';
import { ViewseekserComponent } from './viewseekser/index';
import { TermnpolicyComponent } from './termnpolicy/index';
import { PrivacynpolicyComponent } from './privacynpolicy/index';
import { ProfileeditComponent } from './profileedit/index';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AgmCoreModule } from '@agm/core';
import {MatTabsModule,MatNativeDateModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';

import { ChatComponent } from './chat/index';


@NgModule({
    imports: [
        BrowserModule,
		RouterModule,
        FormsModule,
        HttpModule,
		BrowserAnimationsModule,
		MaterialModule,
        routing,
		BsDropdownModule.forRoot(),
		AgmCoreModule.forRoot({
			apiKey: 'AIzaSyCrn5x64Y9z6ofi5TTtLyHKHqxPMpkidIQ'
		}),
        MatTabsModule,
        MatDatepickerModule,
        MatNativeDateModule
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        JobsComponent,
        FindjobseekersComponent,
        CreatejobComponent,
        LoginComponent,
  //NavbarComponent,
  FooterComponent,
        RegisterComponent,
        MenuComponent,
        My_chatComponent,
        ProfileComponent,
        My_postsComponent,
        My_jobsComponent,
        ViewjobComponent,
        TransactionsComponent,
        ViewseekserComponent,
        TermnpolicyComponent,
        PrivacynpolicyComponent,
        ProfileeditComponent,
	ChatComponent
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        JobService,

        // providers used to create fake backend
        MockBackend,
        BaseRequestOptions
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }