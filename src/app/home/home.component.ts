import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../_models/index';
import { UserService, AlertService, AuthenticationService } from '../_services/index';

@Component({
    selector: 'app-home',
    moduleId: module.id,
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css']
})

export class HomeComponent implements OnInit {
    currentUser: User;
	userDetails: any=[];
    jobs: User[] = [];
   // google maps zoom level
  zoom: number = 14;
  
  // initial center position for the map
  lat: number = 51.673858;
  lng: number = 7.815982;
  user_id: Number = 0;


   clickedMarker(label: string, index: number) {
    //console.log('clicked the marker: ${label || index}');
  }
  
   markers: marker[] = [];

    constructor(private userService: UserService,private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService)
		{
		/* if(!this.authenticationService.isLoggedIn()){
            this.router.navigate(['/']);
        } */
        this.userDetails = JSON.parse(localStorage.getItem('currentUser'));
		if(this.userDetails && this.userDetails.ResponseCode){
			this.user_id = this.userDetails.data.id;
		}
    }

    ngOnInit() {
        this.loadJobListings();
    }
	
    private loadJobListings() {
        //this.userService.getAll().subscribe(users => { this.users = users; });
        this.userService.joblisting(this.user_id).subscribe(
                data => {
                    //this.router.navigate([this.returnUrl]);
					//this.loading = false;
					//this.error_messages = data.Message;
					//return (data.data);
					//console.log(data.mapimageurl);
					
					
					for (var i = 0; i < data.data.length; i++) {
					   if(i==0){
						this.lat=parseFloat(data.data[0].lat);
						this.lng=parseFloat(data.data[0].lng);
					   }
					 
					   this.markers.push({
						lat: parseFloat(data.data[i].lat),
						lng: parseFloat(data.data[i].lng),
						label: data.data[i].job_title,
						draggable: true,
						image:data.mapimageurl+data.data[i].category.image,
					  });
					
					  
					}
					//console.log(this.markers);
					this.jobs = data.data;
                },
                error => {
                    this.alertService.error(error);
                    //this.loading = false;
                });
    }
}
interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
	image?: string;

}