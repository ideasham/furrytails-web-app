import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { My_postsComponent } from './my_posts.component';

describe('My_profileComponent', () => {
  let component: My_postsComponent;
  let fixture: ComponentFixture<My_postsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ My_postsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(My_postsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
