import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, AuthenticationService, UserService } from '../_services/index';
import * as _ from 'underscore';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
	error_messages: string;

		
	constructor(private userService: UserService,private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService)
		{
		}

    ngOnInit() {
		
		// reset login status
		//this.authenticationService.logout();
        if(!_.isEmpty(localStorage.getItem('currentUser'))){
            this.router.navigate(['/']); 
        }
        //this.authenticationService.logout();
        else{
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        }
    }

	
	
    login() {
        this.loading = true;
        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(
                data => {
                    //this.router.navigate([this.returnUrl]);
					this.loading = false;
					this.error_messages = data.Message;
                    if(data.ResponseCode){
                        this.router.navigate(['/']); 
                    }
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
