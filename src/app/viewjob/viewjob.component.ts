import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Job } from '../_models/job';
import { Category } from '../_models/category';
import { User } from '../_models/user';
import { JobService } from '../_services/job.service';

@Component({
	selector: 'app-viewjob',
    moduleId: module.id,
    //template: "hello hi my View job"
    templateUrl: 'viewjob.component.html'
})

export class ViewjobComponent implements OnInit {
    job_user_id: string = '0';
    job_id: string = '0';
    jobdetails: Job[] = [];
    jobcat: Category[] =[];
    jobowner: User[]    =   [];

    constructor(private activatedRoute: ActivatedRoute, private jobservice: JobService){}

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            this.job_user_id = params['userid'];
            this.job_id =   params['jobid'];
            //console.table(params);
          });
    this.viewJobDetails();
    }

  
    private viewJobDetails() {
        //this.userService.getAll().subscribe(users => { this.users = users; });
       this.jobservice.viewJob(this.job_id,this.job_user_id).subscribe(
                data => {
                    this.jobdetails     =   data.data;
                    this.jobcat         =   data.data.category;
                    this.jobowner       =   data.data.user;
                    //console.table(this.jobcat);
                },
                error => {
                   // this.alertService.error(error);
                });
    } 
   
}