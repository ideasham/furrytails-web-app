import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermnpolicyComponent } from './termnpolicy.component';

describe('TermnpolicyComponent', () => {
  let component: TermnpolicyComponent;
  let fixture: ComponentFixture<TermnpolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermnpolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermnpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
