import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { User } from '../_models/index';
import { UserUpdate } from '../_models/update_user';
import { appvar } from '../appvar';

@Injectable()
export class UserService {
    constructor(private http: Http) { }
	 private _apiUrl = appvar.API_URL; // URL to web api
	
	
    getAll() {
        return this.http.get('/api/users', this.jwt()).map((response: Response) => response.json());
    }
	
	joblisting(user_id:Number) {
        //return this.http.get('/api/users', this.jwt()).map((response: Response) => response.json());
		 return this.http.get(this._apiUrl+"/jobs/joblisting?user_id="+user_id).map((response: Response) => {
                let data = response.json();
				return data;
            });
    }

    myapplyedjob(user_id:Number,api_token:String){
        return this.http.get(this._apiUrl+"/jobs/myappliedjobs?api_token="+api_token+"&user_id="+user_id).map((response: Response) => {
            let data = response.json();
            return data;
        });
    }

    mytransactionhistory(user_id:Number,api_token:String){
        user_id =   19;
        return this.http.get(this._apiUrl+"/user/transactionhistory?api_token="+api_token+"&user_id="+user_id).map((response: Response) => {
            let returndata = response.json();
            return returndata;
        });
    }

    myarchivedjobs(user_id:Number,api_token:String){
        user_id =   23;
        return this.http.get(this._apiUrl+'/archivejobs/listing?api_token='+api_token+'&user_id=23&archive_type=2').map((response:Response) => {
            let data = response.json();
            return data;
        });
    }

    getById(id: number) {
        return this.http.get('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    getUserProfile(id: Number){
        return this.http.get(this._apiUrl+"/user/viewprofile?user_id="+id).map((response: Response) => {
            let data = response.json();
            return data;
        });
    }

    create(user: User) {
       // return this.http.post('/api/users', user, this.jwt()).map((response: Response) => response.json());
        return this.http.post(appvar.API_URL+"/user/signup",user);
    }

    update(user: User) {
        return this.http.put('/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
    }

    updateuser(userdata: UserUpdate, user_id:Number, api_token:String) {
        return this.http.post(this._apiUrl+"/user/updateprofile?api_token="+api_token,userdata).map((response: Response) => {
					// login successful if there's a jwt token in the response
					let returndata = response.json();
					return returndata;
				});
    }

    delete(id: number) {
        return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    // private helper methods

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }

   getAllMessages(profileid:Number, api_token:String) {
        return this.http.get(this._apiUrl+"/admin/chat/alllist/"+profileid+"?api_token="+api_token).map((response: Response) => {
		return response.json();
	});
    }

    getChatMessages(myid:Number, profileid:Number, jobid:Number, lastMsgId:Number, api_token:String) {
        return this.http.get(this._apiUrl + '/admin/chat/list/' +myid+ "/" +profileid+ "/" +jobid+ "/"+lastMsgId+"?api_token="+api_token).map((response: Response) => {
		return response.json();
	});
    }

}