import { Injectable, Output, EventEmitter } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { appvar } from '../appvar';
import 'rxjs/add/operator/map'
import * as _ from 'underscore';

@Injectable()
export class AuthenticationService {
    constructor(private http: Http) { }
	 private _apiUrl = appvar.API_URL; // URL to web api
	 
	 @Output() authChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

     private userAuthChanged(status: boolean) {
        this.authChanged.emit(status); //Raise changed event
     }

    login(email: string, password: string) {
         return this.http.post(this._apiUrl+"/user/login", { email: email, password: password })
		//return this.http.post(this._apiUrl+"/user/login",user);
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.ResponseCode) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                   // sessionStorage.setItem('currentUser', JSON.stringify(user));
					this.userAuthChanged(user.ResponseCode);
                } 
				return user;
            }); 

    }
	
	  

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
	
	isLoggedIn(){
       // reset login status
       if(!_.isEmpty(localStorage.getItem('currentUser'))){
        return true;
    }else{
        return false;
        } 
    }
}