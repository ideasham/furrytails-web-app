import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Job } from '../_models/job';
import { appvar } from '../appvar';

@Injectable()
export class JobService {
    constructor(private http: Http) { }
	 private _apiUrl = appvar.API_URL; // URL to web api
	
   
	
	getAllCategory() {
        //return this.http.get('/api/users', this.jwt()).map((response: Response) => response.json());
		 return this.http.get(this._apiUrl+"/category/all").map((response: Response) => {
                let data = response.json();
				return data;
            });
    }
	
	getFindJobSeekers(user_id:Number) {
        //return this.http.get('/api/users', this.jwt()).map((response: Response) => response.json());
		 return this.http.get(this._apiUrl+"/jobs/jobseekers?user_id="+user_id).map((response: Response) => {
                let data = response.json();
				return data;
            });
    }

    viewJob(jobid,userid){
        return this.http.get(this._apiUrl+"/jobs/viewjob?job_id="+jobid+"&user_id="+userid).map((response: Response) => {
            let data = response.json();
            return data;
        });
    }

    removeappliedjob(job_id:number,user_id:Number,api_token:String){
        return this.http.post(this._apiUrl+'/jobs/removeapplied?api_token='+api_token+'&user_id='+user_id+'&job_id='+job_id,{}).map((response: Response) =>{
            let returndata = response.json();
            return returndata;
        });
    }

    applyforjob(job_id:number,user_id:Number,api_token:String){
        return this.http.post(this._apiUrl+"/jobs/applyforjob?api_token="+api_token,{'user_id':user_id,'job_id':job_id}).map((response: Response) =>{
            let returndata = response.json();
            return returndata; 
        });
    }
	
	createjob(job: Job, user_id:Number, api_token:String) {
		//api_token:string = userDetails.api_token;
       // return this.http.post('/api/users', user, this.jwt()).map((response: Response) => response.json());
        return this.http.post(this._apiUrl+"/jobs/addjob?api_token="+api_token,job).map((response: Response) => {
					// login successful if there's a jwt token in the response
					let returndata = response.json();
					return returndata;
				});
    }
  
}