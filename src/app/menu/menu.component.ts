import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../_models/index';
import { UserService } from '../_services/index';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'underscore';
import { AlertService, AuthenticationService } from '../_services/index';
//import { BsDropdownModule } from 'ngx-bootstrap';

@Component({
  selector: 'app-menu, button-types-example',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
	
  isCollapsed: boolean;
  loginLogoutText: string = 'Login';
  sub: Subscription;
  currentUser: User;
  users: User[] = [];
  
   constructor(private router: Router, private authenticationService: AuthenticationService) { 
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

 ngOnInit() {
	 this.setLoginLogoutText();
	 this.sub = this.authenticationService.authChanged
	  .subscribe((loggedIn: boolean) => {
		this.setLoginLogoutText();
	  },
	  (err: any) => console.log(err));
  }
  
  
	loginOrOut() {
		  //if (localStorage.getItem('currentUser')) {
		  if(this.authenticationService.isLoggedIn()){
			  // logged in so return true
			  console.log('run');
			  this.authenticationService.logout();
			  this.setLoginLogoutText();
			  this.router.navigate(['/login']); 
			  return true;
		  }
		  // not logged in so redirect to login page with the return url
		  //this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
		  return false;
	}

	setLoginLogoutText() {
		this.loginLogoutText = !_.isEmpty(localStorage.getItem('currentUser')) ? 'Logout' : 'Login';
		//this.loginLogoutText = _.now();
	}
	

}
