import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { User } from '../_models/index';
import { UserService, AlertService, AuthenticationService, JobService } from '../_services/index';

@Component({
	selector: 'app-profile',
    moduleId: module.id,
    templateUrl: 'profile.component.html',
	styleUrls: ['profile.component.css']
    //template: "Hello Hi"
})

export class ProfileComponent implements OnInit {
    model: any ={};
    currentUser: User;
    users: User[] = [];
    userid:Number   = 0;
    api_token: String = "";
    jsonUser:any    =   [];
    userDetails:any =   [];
    userPayments:any=   [];
    myapplyedjobdata:any;
    myarchivedjobdata:any;
    mytransactionhistorydata:any;
    applyedjobstatus:boolean = false;
    load_spiner              =  true;

    constructor(private userService: UserService,private route: ActivatedRoute,
        private jobservice: JobService,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService)
		{
		if(!this.authenticationService.isLoggedIn()){
            this.router.navigate(['/']);
        }
        this.jsonUser = JSON.parse(localStorage.getItem('currentUser'));
        if(this.jsonUser && this.jsonUser.ResponseCode){
           this.userid = this.jsonUser.data.id; 
           this.api_token = this.jsonUser.data.api_token;
        }
        this.model.first_name   =   '';
        this.model.last_name     =   '';
        this.model.phone        =   '';
        this.model.address      =   '';
        this.model.gender       =   '';      
        this.model.age          =   '';
        this.model.about        =   '';
        this.model.user_job_role=   '';
        this.model.billing_type =   '';
        this.model.price        =   '';
        this.model.profile_picture          =   '';
        this.model.profile_status       =   '';
    }

    ngOnInit() {
        this.loadUserProfile();
        this.jobhistory();
        this.mytransactionhistory();
    }

    private loadUserProfile() {
        this.userService.getUserProfile(this.userid).subscribe(data => { 
           // console.table(data);
            this.userDetails                    =  data.data.userDetail;
            this.userPayments                   =  data.data.userPaymentDetails;
            this.model.first_name               =   this.userDetails.first_name;
            this.model.last_name                =   this.userDetails.last_name;
            this.model.phone                    =   this.userDetails.phone;
            this.model.address                  =   this.userDetails.address;
            this.model.gender                   =   this.userDetails.gender;        
            this.model.age                      =   this.userDetails.age;
			this.model.about                    =   this.userDetails.about;
			this.model.user_job_role            =   this.userDetails.user_job_role;
			this.model.billing_type             =   this.userDetails.billing_type;
            this.model.price                    =   this.userDetails.price;
            this.model.profile_picture          =   this.userDetails.profile_picture;
            this.model.profile_status           =   this.userDetails.profile_status;

            this.load_spiner= false;
         });
    }

    jobhistory(){
        this.appliedjob();
    }

    show_my_archived(){
        this.load_spiner= true;
        this.userService.myarchivedjobs(this.userid,this.api_token).subscribe(data => {
            this.myarchivedjobdata = data.data;
            this.load_spiner= false;
        });
    }

    appliedjob(){
        this.userService.myapplyedjob(this.userid,this.api_token).subscribe(data => {
            this.myapplyedjobdata = data.data;
            //if(this.myapplyedjobdata.length>0){
                this.applyedjobstatus = true;
            //}
        },
    error => {
        alert('unable to connect!');
    });
    }

    removeapplied(job_id){
        this.jobservice.removeappliedjob(job_id,this.userid,this.api_token).subscribe(data=>{
                alert(data.Message);
        });
    }

    archiveapplied(job_id){
    
    }

    mytransactionhistory(){
      this.userService.mytransactionhistory(this.userid,this.api_token).subscribe(data => {
          //console.table(data);
          this.mytransactionhistorydata = data.data;
      });  
    }
}
export class FormFieldOverviewExample {}


