import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../_models/index';
import { UserService, JobService, AlertService, AuthenticationService } from '../_services/index';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';


@Component({
	selector: 'app-createjob',
    moduleId: module.id,
    templateUrl: 'createjob.component.html'
})

export class CreatejobComponent implements OnInit {
	model: any = {};
    loading = false;
    currentUser: User;
    userDetails: any=[];
	user_id: Number = 0;
	api_token: String = "";
	reponse_message: string;
    categoryList: User[] = [];
    currentdatetime =   new Date();
    events: string[] = [];
	
	
    users: User[] = [];

    constructor(private userService: UserService, private jobService: JobService,private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService)
		{
		/* if(!this.authenticationService.isLoggedIn()){
            this.router.navigate(['/']);
        }  */
        this.userDetails = JSON.parse(localStorage.getItem('currentUser'));	
		if(this.userDetails.ResponseCode){
			this.user_id = this.userDetails.data.id;
			this.api_token = this.userDetails.data.api_token;
			this.model.user_id = this.user_id;
		}
    }

	ngOnInit() {

        this.model.user_id          =   this.user_id;
        this.model.job_title        =   'job title';
        //this.model.start_date       =   new Date();
        //this.model.end_date         =   '2010-01-01';
        this.model.start_time       =   '13:00:00';
        this.model.end_time         =   '16:00:00';
        this.model.cat_id           =   '2';
        this.model.area             =   'area name';
        this.model.description      =   'bla bla bla bla';
        this.model.billing_type     =   'hourly';
        this.model.price            =   '120';
        this.model.image            =   '';

        this.loadAllCategory();
    }
	
	 private loadAllCategory() {
        this.jobService.getAllCategory().subscribe(data => { 
            this.categoryList    =  data.data;
         });
    }

    private setToday() {
        console.log(this.currentdatetime);
    }

    addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        this.events.push(`${type}: ${event.value}`);
        console.table(this.events);
      }
	
	createjob() {
        console.table(this.model);
        this.loading = true;/*
        this.jobService.createjob(this.model, this.user_id, this.api_token)
            .subscribe(
                data => { 
                    //this.alertService.success('Job Created successful', true);
                    console.log(data);
                    if(data.ResponseCode){
					    this.reponse_message = "Job added!";
                        this.loading = false;
                    }else{
                        this.reponse_message = "Unable to process your request!";
                        this.loading = false;
                    }
                    //this.router.navigate(['/home']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });*/
    }
	
	
}