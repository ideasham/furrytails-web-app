import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../_models/index';
import { UserService, JobService, AlertService, AuthenticationService } from '../_services/index';

@Component({
	selector: 'app-jobs',
    moduleId: module.id,
    templateUrl: 'jobs.component.html'
})

export class JobsComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    loading = false;
    userDetails: any=[];
	user_id: Number = 0;
    api_token: String = "";
    joblistings:any;

    constructor(private userService: UserService,private jobService: JobService,private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService)
		{
		/* if(!this.authenticationService.isLoggedIn()){
            this.router.navigate(['/']);
        } */
        this.userDetails = JSON.parse(localStorage.getItem('currentUser'));	
		if(this.userDetails && this.userDetails.ResponseCode){
			this.user_id = this.userDetails.data.id;
			this.api_token = this.userDetails.data.api_token;
		}
    }

    ngOnInit() {
        this.loadAllUsers();
    }

    deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
    }

    private loadAllUsers() {
        //this.userService.getAll().subscribe(users => { this.users = users; });
        this.userService.joblisting(this.user_id).subscribe(
                data => {
                    console.table(data.data);
                    this.joblistings = data.data;
                },
                error => {
                    this.alertService.error(error);
                });
    }

    public applyforjob(jobid){
        console.log(this.user_id+' -- '+jobid+' --- '+this.api_token);
        this.jobService.applyforjob(jobid,this.user_id,this.api_token).subscribe(
            data => {
                //{"ResponseCode":false,"Message":"You are not a job Seeker yet, Set Yorself as a Jobseeker from your profile first.","data":"youarenotseeker"}
                if(data.ResponseCode){
                    alert(data.Message);
                }
                else{
                    alert(data.Message);
                }
            },
            error => {
                this.alertService.error(error);
            }
        );
    }

    public clicked(){
        console.log('killer');
    }
}