import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacynpolicyComponent } from './privacynpolicy.component';

describe('PrivacynpolicyComponent', () => {
  let component: PrivacynpolicyComponent;
  let fixture: ComponentFixture<PrivacynpolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivacynpolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacynpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
