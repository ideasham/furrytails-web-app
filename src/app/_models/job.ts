export class Job{
    'id':Number;
    'job_title':string;
    'description':string;
    'start_date':string;
    'end_date':string;
}