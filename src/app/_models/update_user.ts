export class UserUpdate {
    id: number;
    username: string;
    password: string;
    first_name: string;
    last_name: string;
    ResponseCode: Boolean;
    user_id: number;
    user_job_role: number;
    img: string;
    age: number;
    gender: string;
    phone: number;
    billing_type: number;
    price: string;
    address: string;

}