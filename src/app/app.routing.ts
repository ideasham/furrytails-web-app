import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { JobsComponent } from './jobs/jobs.component';
import { FindjobseekersComponent } from './findjobseekers/findjobseekers.component';
import { CreatejobComponent } from './createjob/createjob.component';
//import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { AuthGuard } from './_guards/index';
import { ProfileComponent } from './profile/index';
import { My_chatComponent } from './my_chat/index';
import { My_postsComponent } from './my_posts/index';
import { My_jobsComponent } from './my_jobs/index';
import { ViewjobComponent } from './viewjob/index';
import { TransactionsComponent } from './transactions/index';
import { ViewseekserComponent } from './viewseekser/index';
import { TermnpolicyComponent } from './termnpolicy/index';
import { PrivacynpolicyComponent } from './privacynpolicy/index';
import { ProfileeditComponent } from './profileedit/index';
import { ChatComponent } from './chat/index';

const appRoutes: Routes = [
    { path: '', component: HomeComponent},
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
	{ path: 'home',   component: HomeComponent  },
	{ path: 'jobs',   component: JobsComponent  },
	{ path: 'findjobseekers',   component: FindjobseekersComponent  },
    { path: 'createjob',   component: CreatejobComponent  },
    { path: 'profile',   component: ProfileComponent  },
    { path: 'mychat', component: My_chatComponent },
    { path: 'myposts', component: My_postsComponent },
    { path: 'myjobs', component: My_jobsComponent },
    { path: 'viewjob/:jobid/:userid', component: ViewjobComponent},
    { path: 'transactions', component: TransactionsComponent},
    { path: 'viewseekser', component: ViewseekserComponent },
    { path: 'term_and_policy', component: TermnpolicyComponent},
    { path: 'privacy_policy', component: PrivacynpolicyComponent},
    { path: 'edituser', component: ProfileeditComponent },

    { path: 'chat', component: ChatComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);