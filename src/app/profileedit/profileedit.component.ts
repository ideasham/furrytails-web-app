import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../_models/index';
import { UserService, AlertService, AuthenticationService } from '../_services/index';

@Component({
	selector: 'app-my_posts',
    moduleId: module.id,
    //template: "hello hi my posts"
    templateUrl: 'profileedit.component.html'
})

export class ProfileeditComponent implements OnInit {
    model: any = {};
    loading = false;
    currentUser: User;
    userDetails: any=[];
	user_id: Number = 0;
	api_token: String = "";
	reponse_message: string;
    categoryList: User[] = [];
    userPayments:any=   [];
    load_spiner              =  true;

    constructor(private userService: UserService,private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService)
		{
		if(!this.authenticationService.isLoggedIn()){
            this.router.navigate(['/']);
        }
        this.userDetails = JSON.parse(localStorage.getItem('currentUser'));	
		if(this.userDetails.ResponseCode){
			this.user_id = this.userDetails.data.id;
			this.api_token = this.userDetails.data.api_token;
			this.model.user_id = this.user_id;
        }
                this.model.first_name           =   '';
                this.model.last_name            =   '';
                this.model.phone                =   '';
                this.model.address              =   '';
                this.model.gender               =   '';
                this.model.age                  =   '';
				this.model.about                =   '';
				this.model.user_job_role        =   '';
				this.model.billing_type         =   '';
                this.model.price                =   '';
                this.model.profile_picture                  =   '';
                this.model.profile_status       =   '';
                this.model.img       =   '';
    }

    ngOnInit() {
        this.loadUserProfile();
    }

    private loadUserProfile() {
        this.userService.getUserProfile(this.user_id).subscribe(data => { 
                this.userDetails        =  data.data.userDetail;
                this.userPayments       =  data.data.userPaymentDetails;
                this.model.first_name   =   this.userDetails.first_name;
                this.model.last_name    =   this.userDetails.last_name;
                this.model.phone        =   this.userDetails.phone;
                this.model.address      =   this.userDetails.address;
                this.model.gender       =   this.userDetails.gender;        
                this.model.age          =   this.userDetails.age;
				this.model.about        =   this.userDetails.about;
				this.model.user_job_role=   this.userDetails.user_job_role;
				this.model.billing_type =   this.userDetails.billing_type;
                this.model.price        =   this.userDetails.price;
                this.model.profile_picture      =   this.userDetails.profile_picture;
                this.model.profile_status       =   this.userDetails.profile_status;
                this.load_spiner        =   false;
         });
    }

    updateprofile(){
        this.load_spiner        =   true;
          console.log(this.model);
            this.userService.updateuser(this.model,this.user_id,this.api_token).subscribe(data=>{
                this.loadUserProfile();
            });
         }

         changeListener($event) : void {
            this.readThis($event.target);
          }
          
          readThis(inputValue: any): void {
            var file:File = inputValue.files[0];
            var myReader:FileReader = new FileReader();
          
            myReader.onloadend = (e) => {
              this.model.img = myReader.result;
            }
            myReader.readAsDataURL(file);
    }

  

   
}