(function (global) {
    System.config({
        'paths': {
    'npm:':'node_modules/'
},

'map': {
     'ng2-datetime-picker': 'npm:ng2-datetime-picker',
},
'packages':  {
     'ng2-datetime-picker': 'dist/ng2-datetime-picker.umd.js'
 }
});
})(this);